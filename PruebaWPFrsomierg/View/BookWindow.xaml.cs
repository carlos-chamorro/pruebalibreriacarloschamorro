﻿using PruebaWPFrsomierg.Controller;
using PruebaWPFrsomierg.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PruebaWPFrsomierg
{

    public partial class BookWindow : Window
    {
        BookController bc;

        public BookWindow()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            SetController();
        }

        public Book GetData()
        {
            Book book = new Book();
            book.Title = BookNameTextBox.Text;
            book.Author = NameAuthorTextBox.Text;
            book.Año = YearTextBox.Text;
            book.categoria = StateCombobox.SelectedValue.ToString();
            return book;
        }

        public void SetData(Book book)
        {
            BookNameTextBox.Text = book.Title;
            NameAuthorTextBox.Text = book.Author;
            YearTextBox.Text = book.Año;
            StateCombobox.SelectedValue = book.categoria;
        }

        protected void SetController()
        {
            bc = new BookController(this);
            this.SaveButton.Click += new RoutedEventHandler(bc.BookEventHandler);
            this.OpenButton.Click += new RoutedEventHandler(bc.BookEventHandler);
        }

        public static implicit operator BookWindow(Book v)
        {
            throw new NotImplementedException();
        }
    }
}
