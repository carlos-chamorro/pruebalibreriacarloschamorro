﻿using Microsoft.Win32;
using PruebaWPFrsomierg.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace PruebaWPFrsomierg.Controller
{
    public class BookController
    {
        BookWindow bwindow;
        SaveFileDialog sfdialog;
        OpenFileDialog ofdialog;
        private BookWindow bookWindow;

        public BookController(Book window)
        {
            bwindow = window;
            sfdialog = new SaveFileDialog();
            ofdialog = new OpenFileDialog();
        }

        public BookController(BookWindow bookWindow)
        {
            this.bookWindow = bookWindow;
        }

        public void BookEventHandler(object sender, RoutedEventArgs e)
        {
            Button B = (Button)sender;
            switch (B.Name)
            {
                case "SaveButton":
                    SaveData();
                    break;
                case "OpenButton":
                    OpenFile();
                    break;
            }
        }

        private void OpenFile()
        {
            ofdialog.Filter = "Xml File (*.xml)|*.xml";
            if (ofdialog.ShowDialog() == true)
            {
                Book b = new Book();
                bwindow.SetData(b.FromXml(ofdialog.FileName));
            }
        }

        private void SaveData()
        {
            sfdialog.Filter = "Xml File (*.xml)|*.xml";
            if (sfdialog.ShowDialog() == true)
            {
                Book b = bwindow.GetData();
                b.ToXml(sfdialog.FileName);
            }
        }
    }
